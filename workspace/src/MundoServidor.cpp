// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <string>
#include  <pthread.h>


#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h> 
#include <unistd.h>
#include <sys/mman.h>
#include <signal.h>


char texto[100] ="jugador1\n";
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////



void* hilo_comandos(void* d)
{
CMundoServidor* p = (CMundoServidor*) d;
p->recibe();
}

void CMundoServidor::recibe(){
//fifo_cliente =  open ("FIFO_CLIENTE",O_RDONLY);
while(1){
usleep(10);
//fifo
char cadena[100];
//read(fifo_cliente,cadena,sizeof(cadena));



//socket

unsigned char key;
s_comunicacion.Receive(cadena,sizeof(cadena));

sscanf(cadena,"%c",&key);
if(key=='s') jugador1.velocidad.y=-4;
if(key=='w')jugador1.velocidad.y=4;
if(key=='l')jugador2.velocidad.y=-4;
if(key=='o')jugador2.velocidad.y=4;
	}
}


CMundoServidor::CMundoServidor()
{

	Init();
fd = creat("tuberia", 0777);
}

CMundoServidor::~CMundoServidor()
{
unlink("tuberia");
//close(fifo_servidor);
//close(fifo_cliente);
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	




	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

int n;
    char  c[50]  ;
        //write(fd,texto,sizeof(texto));
 sprintf(c,"\n jugador 2  marca 1 punto, lleva %d  ",puntos2);
n = strlen(c);
//strcpy(texto,a);
//char d[100] ="\n jugador 1 maca un 1 punto,lleva ";
//strcpy(texto,a);
// write(fd,d,sizeof(d));
//write(fd,c,sizeof(char));
write(fd,c,n);

if(puntos2 == 3) exit(1);


	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
    char  c[50]  ;
int n ;        //write(fd,texto,sizeof(texto));
sprintf(c,"\n jugador 1  marca 1 punto, lleva %d  ",puntos1);
n = strlen(c);
//strcpy(texto,a);
//char d[100] ="\n jugador 2 maca un 1 punto,lleva "; 
//strcpy(texto,a);
// write(fd,d,sizeof(d));
//write(fd,c,sizeof(char));
write(fd,c,n);

if (puntos1 == 3) exit(1);


	}

//envios 
char cadena[200];
	sprintf(cadena,"%f %f %f %f %f %f %f %f %f %f %d %d",
		esfera.centro.x,esfera.centro.y, 
		jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,
		jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, 
		puntos1, puntos2);
//	write(fifo_servidor,cadena,sizeof(cadena));
s_comunicacion.Send(cadena,sizeof(cadena));

}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
//	switch(key)
//	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
  
//	case 's':jugador1.velocidad.y=-4;break;
//	case 'w':jugador1.velocidad.y=4;break;
//	case 'l':jugador2.velocidad.y=-4;break;
//	case 'o':jugador2.velocidad.y=4;break;

//	}
}

void CMundoServidor::Init()
{
char ip[]="127.0.0.1";
        if(s_conexion.InitServer(ip,8000)==-1)
                printf("error abriendo el servidor\n");
        s_comunicacion=s_conexion.Accept();
        //RECIBIMOS EL NOMBRE DEL CLIENTE
        char nombre[50];
        s_comunicacion.Receive(nombre,sizeof(nombre));
        printf("%s se ha conectado a la partida.\n",nombre);



	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;


//fifos

//fifo_servidor= open ("FIFO_SERVIDOR", O_WRONLY);

//threads

	pthread_attr_init(&atrib);
	pthread_attr_setdetachstate(&atrib, PTHREAD_CREATE_JOINABLE);

	pthread_create(&thid1, &atrib, hilo_comandos, this);

}
